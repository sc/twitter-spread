source("model.R")
library(igraph)

#transfrom a cascade made of numeric index into character
#if not igraph will fill missing vertices with fake one
tocharact <- function(cascade,i){
    if(is.null(dim(cascade)))
        t(paste0("c",i,"-",cascade[1:2]))
    else
        t(apply(cascade,1,function(r)paste0("c",i,"-",r[1:2])))
}
for(i in 1:length(alllayout)){
    withCallingHandlers({layout_as_tree(allsubgraph[[i]])},warning=function(w) print(i))
}


test=cascades3D(N=1000,IC=10,betadistrib=rep(1,1000),R=5,utility=runif(5),summary=F,repetition=100,Nmax=.2,dtime = 1,lambda_c = 1)
ca=allt$cascades
cid=test$summary

i=1
tocharact(oner,1)
oner=ca[[i]][,c(1,2)]
ig=graph_from_edgelist(oner)
plot(ig2,layout=layout_as_tree)
alledges=do.call("rbind",sapply(1:length(ca),function(i)tocharact(ca[[i]],i)))
graphall=graph_from_edgelist(alledges)
plot(,layout=layout_as_tree)
oner2=ca[[2]][,c(1,2)]
oner2=t(apply(oner2,1,as.character))
ig2=graph_from_edgelist()
plot(allsubgraph[[1]],layout=layout_as_tree,vertex.color=)

    allsubgraph=lapply(1:length(ca),function(j)graph_from_edgelist(tocharact(ca[[j]],j)))

lapply(alllayout,function(l)max(l[,2])-min(l[,2]))
max(sapply(ca,function(g)nrow(g)))


cols=grey.colors(length(unique(cid$U)))
names(cols)=sort(unique(cid$U))

#max round = n * IC * N
i=0
for(t in round(seq(1,(100*.2*1000),length.out=100))){
    print(t)

    tmp=lapply(ca,function(cc)if(length(cc[cc[,3]<t,])==0)  null  else cc[cc[,3]<t,])
    allsubgraph=lapply(1:length(tmp),function(j)graph_from_edgelist(tocharact(tmp[[j]],j)))
    png(file.path("gifmaking",sprintf("%05d_cascades.png",i)),width=2.5*480)
    par(mfrow=c(1,10),mar=c(0,0,0,0),oma=c(0,0,0,0))
    lapply(order(cid$U),function(g)plot(allsubgraph[[g]],layout=layout_as_tree,vertex.color=cols[as.character(cid$U[g])],vertex.label=NA))
    dev.off()
    i=i+1
}




