# Twitter Spread R-package
General folder for different things.
Folder content:
* `abcdir/` : set of R code and other script to do some simple rejection Approximate Bayesian Computation.
* `doc/` :  some reflections, summary, abstracts, presentation and theoretical exploration. For the documentation about the R commands see : `spread/doc`.
* `spread/` : a tentative of R package with the models developed and the tools used to analyse them.
* `tools/` : some random more or less useful scripts.
* `vosoughi_original/` : folder with quick-n-dirty scripts and the original scripts by Vosoghi et al. to explore their dataset. Some of the data are also available directly within the package `spread`.

Ideally most of the code within `tools` and most of the latex in `doc/` should end up as R CRAN vignette for the package

